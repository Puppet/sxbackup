# Run a backup on a cronjob
define sxbackup::cron(
  $source      = undef,
  $mail        = undef,
  $log_ident   = undef,
  $cron        = undef,
) {
  validate_string($source, $log_ident)

  if ! $cron {
    fail('Cron parameters are required for cronjobs')
  }

  if ! $source {
    fail('Source volume is required for backups and snapshots in cronjobs')
  }

  if $mail {
    if is_bool($mail) {
      $m = '-m'
    } else {
      $m = "-m ${mail}"
    }
  }

  if $log_ident {
    $li = "-li ${log_ident}"
  }

  $cron_default = {
    command => "${::sxbackup::params::bin} -q run ${m} ${li} -- ${source}",
    user    => root,
  }

  $cron_params = merge($cron_default, $cron)

  create_resources( 'cron', { "sxbackup-${title}" => $cron_params })
}
