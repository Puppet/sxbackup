# Configure the environment
class sxbackup::params {
  include 'stdlib'

  $bin = '/usr/local/bin/btrfs-sxbackup'

  $packages = ['python3', 'python3-pip', 'lzop', 'pv']
  package { $packages: ensure => 'installed' }

  $btrfs_progs = $::osfamily ? {
    'Debian'  => 'btrfs-tools',
    default => 'btrfs-progs',
  }
  package { $btrfs_progs: ensure => 'installed' }

  exec { 'Installing btrfs-sxbackup':
    command => '/usr/bin/pip3 install btrfs-sxbackup',
    creates => $bin,
  }

}
