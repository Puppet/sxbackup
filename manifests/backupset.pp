# Create several sxbacbup::backup instances
# with default settings and one cronjob
define sxbackup::backupset (
  $source_base           = undef,
  $destination_base      = undef,
  $source_retention      = undef,
  $destination_retention = undef,
  $compress              = false,
  $cron                  = undef,
  $mail                  = undef,
  $log_ident             = undef,
  $volumes               = undef,
) {
  validate_string($log_ident)
  validate_bool($compress)

  include 'sxbackup::params'

  $backup_defaults = {
    source_base           => $source_base,
    destination_base      => $destination_base,
    source_retention      => $source_retention,
    destination_retention => $destination_retention,
    compress              => $compress,
  }

  if is_array($volumes) {
    $sources = $volumes
    $volumes.each |$source| {
      create_resources('sxbackup::backup', {$source => {'source' => $source}}, $backup_defaults)
    }
  } elsif is_hash($volumes) {
    $sources = $volumes.map |$source,$destination| {
      create_resources('sxbackup::backup', {$source => {'source' => $source, 'destination' => $destination}}, $backup_defaults)
      $source
    }
  } else {
    fail('\'volumes\' must be either an array or a hash')
  }





  if $cron {
    sxbackup::cron{ $title:
      mail      => $mail,
      log_ident => $log_ident,
      cron      => $cron,
      source    => $sources.join(' '),
    }
  }

}
