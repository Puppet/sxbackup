# Configure and plan a backup
define sxbackup::backup(
  $source                = undef,
  $source_base           = undef,
  $destination           = undef,
  $destination_base      = undef,
  $source_retention      = undef,
  $destination_retention = undef,
  $compress              = false,
  $cron                  = undef,
  $mail                  = undef,
  $log_ident             = undef,
) {

  validate_string(
    $source,
    $source_base,
    $destination,
    $destination_base,
    $log_ident
  )
  validate_bool($compress)

  if ! $source {
    fail('Source volume is required for backups and snapshots')
  }

  # Parameters for 'init'
  if $source_retention {
    $sr = "-sr ${source_retention}"
  }

  if $destination_retention {
    $dr = "-dr ${destination_retention}"
  }

  if $compress {
    $c = '-c'
  }

  if $destination {
    $fulldestination = "${destination_base}${destination}"
  }

  if $source {
    $fullsource = "${source_base}${source}"
  }

  exec { "Intial backup for ${title}":
    command => "${::sxbackup::params::bin} init ${sr} ${dr} ${c} \
                -- ${fullsource} ${fulldestination}",
    creates => "${fullsource}/.sxbackup"
  }

  if $cron {
    sxbackup::cron{ $title:
      mail      => $mail,
      log_ident => $log_ident,
      cron      => $cron,
      source    => $source,
    }
  }

}
