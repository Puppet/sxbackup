# Main sxbackup class with shorthands for config and backups
class sxbackup(
  $config = undef,
  $backups = undef,
) {
  include 'sxbackup::params'

  if $config {
    class { 'sxbackup::config':
      mail                  => $config['mail'],
      log_ident             => $config['log_ident'],
      source_retention      => $config['source_retention'],
      destination_retention => $config['destination_retention'],
    }
  }
  if $backups {
    create_resources('sxbackup::backups', {'backups' => $backups})
  }
}
