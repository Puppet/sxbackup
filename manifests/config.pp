# Create a config file for btrfs-sxbackup
class sxbackup::config (
  $mail                  = undef,
  $source_retention      = undef,
  $destination_retention = undef,
  $log_ident             = undef,
) {

  file{ 'btrfs-sxbackup.conf':
    path    => '/etc/btrfs-sxbackup.conf',
    content => epp('sxbackup/btrfs-sxbackup.conf.epp', {
      'mail'                  => $mail,
      'source_retention'      => $source_retention,
      'destination_retention' => $destination_retention,
      'log_ident'             => $log_ident,
    })
  }
}
